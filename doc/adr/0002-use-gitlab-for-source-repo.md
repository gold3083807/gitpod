# 2. use Gitlab for source Repo

Date: 2023-02-13

## Status

Accepted

## Context

깃허브는 온 프레미스로 사용 할 수 없음.
깃랩은 호스팅이 가능함


## Decision

우리는 깃랩을 클라우드로 사용이 가능함.

## Consequences

우리는 git 클라우드의 MVP 버전을 사용
인프라팀은 깃랩 서버를 활용
클라우드로 온프레미스로 결정
