# 3. VSCode for IDE

Date: 2023-02-13

## Status

Accepted

## Context

IDE
- vscode  : free
- intelij : commercial
- eclise  : free

vs code가 가장 유명해서 사용하기로 함

## Decision

  우리는 vscode를 사용한다.

## Consequences

QA팀은 코딩컨벤션을 vs 코드에 세팅하여 배포하라